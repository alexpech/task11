import unittest
import task11


class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(task11.maxProduct(["abcw", "baz", "foo", "bar", "xtfn", "abcdef"]), 16)
        self.assertEqual(task11.maxProduct(["a", "ab", "abc", "d", "cd", "bcd", "abcd"]), 4)
        self.assertEqual(task11.maxProduct(["a", "aa", "aaa", "aaaa"]), 0)
        self.assertEqual(task11.maxProduct(["a", "b", "c", "d"]), 1)
        self.assertEqual(task11.maxProduct(["aewz", "ab", "cdy" ]), 12)


if __name__ == '__main__':
    unittest.main()
