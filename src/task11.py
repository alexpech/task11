

def maxProduct(words):
    pairs = getPairs(words)

    pair = getMaxPair(pairs)

    print(pair)

    return getProduct(pair)


def getPairs(words):
    pairs = []
    for i in range(len(words)):
        for j in range(i, len(words)):
            if not hasCommonLetters(words[i], words[j]):
                pair = (words[i], words[j])
                pairs.append(pair)
    return pairs


def hasCommonLetters(word1, word2):
    chars1 = set(word1)
    chars2 = set(word2)
    return len(chars1.intersection(chars2)) != 0


def getMaxPair(pairs):
    max = 0
    result = ("", "")

    for pair in pairs:
        product = getProduct(pair)
        if product > max:
            max = product
            result = pair

    return result


def getProduct(pair):
    return len(pair[0]) * len(pair[1])



